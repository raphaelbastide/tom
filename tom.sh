#!/bin/bash

############################---Description---###################################
# Tom is a Pomodoro Bash script                                                #
#                                                                              #
# Original script Summary : Show a progress bar GUI on terminal platform       #
# Original Creator        : haikieu2907@gmail.com                              #
# Created date            : Aug 12,2014                                        #
# Hacked by               : Raphaël Bastide                                    #
#                                                                              #
################################################################################

TIME=45
NETWORK=false
SKIP_TIME=0

# Function to display script usage information
function show_help() {
  echo "Tom - Pomodoro Bash Script"
  echo "Usage: tom [-t TIME] [-n]"
  echo
  echo "Options:"
  echo "  -t TIME   Set the time in minutes for the Pomodoro session in minutes (default: 45)"
  echo "  -n        Disable network during the Pomodoro session (default: active)"
  echo "  -s TIME   Skip a custom amount of time (in minute)"
  echo
  echo "Description:"
  echo "  Tom is a Pomodoro Bash script that displays a progress bar on the terminal."
  echo "  It can be used to time Pomodoro sessions and optionally disable the network."
  exit 0
}

# Parse command line arguments
while getopts "t:s:nh" opt; do
  case $opt in
    t)
      TIME=$OPTARG
      ;;
    n)
      NETWORK=true
      ;;
    s)
      SKIP_TIME=$OPTARG
      ;;
    h)
      show_help
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
  esac
done

# Function before exit
cleanup() {
  if $NETWORK; then
    nmcli radio wifi on
  fi
  echo
  exit 0
}

# Set trap to call cleanup function on script exit (Ctrl+C)
trap cleanup INT

# Main script logic
echo "Time: $TIME minutes"
echo "Network: $NETWORK"
echo "Skipping: $SKIP_TIME minutes"

if $NETWORK; then
  echo "Disabling network…"
  nmcli radio wifi off
else
  echo "Network will not be disabled"
fi

#Background Colors
E=$(tput sgr0);    R=$(tput setab 1); G=$(tput setab 2); Y=$(tput setab 3);
B=$(tput setab 4); M=$(tput setab 5); C=$(tput setab 6); W=$(tput setab 7);
function e() { echo -e "$E"; }
function x() { echo -n "$E  "; }
function r() { echo -n "$R  "; }
function g() { echo -n "$G  "; }
function y() { echo -n "$Y  "; }
function b() { echo -n "$B  "; }
function m() { echo -n "$M  "; }
function c() { echo -n "$C  "; }
function w() { echo -n "$W  "; }

#putpixels
function u() {
    h="$*";o=${h:0:1};v=${h:1};
    for i in `seq $v`
    do
        $o;
    done
}

# Tomato!
img="\
 e1 e1 e1 x7 g5 e1 x8 g5 r5 e1 x8 r4 g1 r7 e1 x5 r12 w1 r3 e1 x5 r11 w3 r2 e1 x4 r13 w1 r3 e1 x4 r17 e1 x4 r17 e1 x4 r17 e1 x4 r3 x1 r3 x1 r9 e1 x5 r16 e1 x5 r3 x3 r9 x0 e1 x6 r12 e1 x8 r7 e1 e1 e1 e1 e1 e1
"

for n in $img
do
    u $n
done
e

# delay executing script
function delay(){
  sleep 60;
}

# print out executing progress
NOW=$(date +"%T")
RED='\033[00;31m'
GREEN='\033[00;32m'
YELLOW='\033[00;33m'
BLUE='\033[00;34m'
PURPLE='\033[00;35m'
CYAN='\033[00;36m'
LIGHTGRAY='\033[00;37m'
ICON='emblem-ok'

# Progress function
function progress() {
  local duration=$1
  local total_blocks=$((duration * 60 / 2))  # Adapt progress bar to TIME argument
  local bar_size=40
  local bar_char_done="#"
  local bar_char_todo="-"
  local bar_percentage_scale=2
  local start_time=$(date +%s)

  echo -ne "Progress : [\c"  # "\c" prevents new line

  for ((i = 0; i < total_blocks; i++)); do
    #TODO : adapt the bar with SKIP_TIME
    local percent=$((i * 100 / total_blocks))
    local done=$(bc <<< "scale=0; ($bar_size * $percent / 100)")
    local todo=$((bar_size - done))
    done_sub_bar=$(printf "%${done}s" | tr " " "${bar_char_done}")
    todo_sub_bar=$(printf "%${todo}s" | tr " " "${bar_char_todo}")

    echo -ne "\rProgress : [${done_sub_bar}${todo_sub_bar}] ${percent}%"

    local elapsed_time=$(( $(date +%s) - start_time + SKIP_TIME * 60))
    # local remaining_time=$(( duration * 60 - elapsed_time + SKIP_TIME * 60))
    echo -ne " ($((elapsed_time / 60)) min $((elapsed_time % 60)) sec) \r\c"
    sleep 2  # Wait for 2 seconds to simulate 1 minute
  done

  echo -e "\n"
  notify-send -i $ICON -t 8000 "Tom says" "Take a break!";
  cleanup
}

echo -e $GREEN
echo "⌚ Tom STARTED"
echo -e $RED
progress $TIME

echo
