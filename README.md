# Tom

Minimal [Pomodoro technique](https://en.wikipedia.org/wiki/Pomodoro_Technique) bash script. Tested on GNU/Linux only.

## Options

- `-t`: Time in minutes for the Pomodoro session in minutes (default: 45)
- `-n`: Disable network during the Pomodoro session

![Screenshot](screenshot.png)

## Thanks!

This script is a fork of [shell-progressbar](https://github.com/haikieu/shell-progressbar) by Mr.Kieu

## License

[MIT](http://opensource.org/licenses/MIT)
